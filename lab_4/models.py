from django.db import models
from django.utils import timezone
# Create your models here.

class Schedule(models.Model):
    activity = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    day = models.CharField(max_length=9)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=100)