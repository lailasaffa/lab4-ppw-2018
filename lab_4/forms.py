from django import forms
class Schedule_Form(forms.Form):
    error_messages = {
        'required': 'Required Field',
    }
    day_attrs={
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Day of the Activity'
    }
    date_attrs={
        'type':'date',
        'class': 'form-control',
        'placeholder': 'Date of the Activity'

    }
    time_attrs={
        'type': 'time',
        'class': 'form-control',
        'placeholder': 'Time of the Activity'
    }
    activity_attrs={
        'type':'text',
        'class':'form-control',
        'placeholder':'Name of the Activity'
    }
    place_attrs={
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Place of the Activity'
    }
    category_attrs={
        'type':'text',
        'class':'form-control',
        'placeholder':'Category of the Activity'
    }
    activity = forms.CharField(label='Nama Kegiatan', required=True,max_length=100,
            widget=forms.TextInput(attrs=activity_attrs))
    category = forms.CharField(label='Kategori', required=True,max_length=100,
            widget=forms.TextInput(attrs=category_attrs))
    day = forms.CharField(label='Hari',required=True,max_length=9,
            widget=forms.TextInput(attrs=day_attrs))
    date = forms.DateField(label='Tanggal',required=True,
            widget=forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label='Waktu',required=True,
            widget=forms.TimeInput(attrs=time_attrs))
    place = forms.CharField(label='Tempat', required=True,max_length=100,
            widget=forms.TextInput(attrs=place_attrs))
    
            