from django.urls import path
from . import views

urlpatterns = [
 path(r'', views.home, name='home'),
 path(r'profile/',views.profile, name='profile'),
 path(r'journal/',views.journal, name='journal'),
 path(r'workshop/',views.workshop, name='workshop'),
 path(r'contact/',views.contact, name='contact'),
 path(r'guestbook/',views.guestbook, name='guestbook'),
 path(r'Add_Schedule/', views.add_schedule, name='Add_Schedule'),
 path(r'schedule_result/', views.schedule_result, name='schedule_result'),
 path(r'schedule_post/', views.schedule_post, name = 'schedule_post'),
 path(r'schedule_resultclr/', views.schedule_reset, name='Schedule_Reset')
]

