from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Schedule_Form
from .models import Schedule

def home(request):
    return render(request, 'home.html')

def profile(request):
    return render(request, 'profile.html')

def journal(request):
    return render(request, 'journal.html')

def workshop(request):
    return render(request, 'workshop.html')

def contact(request):
    return render(request, 'contact.html')

def guestbook(request):
    return render(request, 'guestbook.html')


response={}
def schedule_post(request):
    response['add_schedule'] = Schedule_Form
    html = 'schedule_post.html'
    return render(request, html, response)

def add_schedule(request):
    form = Schedule_Form(request.POST or None)
    if(request.method == 'POST'):
        response['activity'] = request.POST['activity'] if request.POST['activity'] != "" else "Activity"
        response['category'] = request.POST['category']
        response['day'] = request.POST['day']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['place'] = request.POST['place'] if request.POST['place'] != "" else "tba"
        
        schedules = Schedule(activity=response['activity'], category=response['category'], day=response['day'],
        date=response['date'], time=response['time'], place=response['place'])
        schedules.save()
        
        return HttpResponseRedirect('/schedule_result')
    else:
        return HttpResponseRedirect('')

def schedule_result(request):
    schedules = Schedule.objects.all()
    response['schedules'] = schedules
    html = 'schedule_result.html'
    return render(request, html, response)

def schedule_reset(request):
    reset = Schedule.objects.all().delete()
    return HttpResponseRedirect('/schedule_result')